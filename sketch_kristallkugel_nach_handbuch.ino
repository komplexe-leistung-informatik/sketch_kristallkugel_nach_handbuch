// Befehlsbibliothek für das LCD
#include <LiquidCrystal.h>    

// Initialisieren der Ports des LCD 
//    D5 (Datenbus)
//    D4 (Datenbus)
//    R/W (Read or Write)
//    RS (Bestimmung ob Datenbit als Befehl oder Zeichendaten interpretiert werden)
//    Vee (Kontrastregelung)
//    Vdd (Stromversorgung))
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);          

// switchPin = Pin 6 am Arduino - verbunden mit einem Kippschalter
const int switchPin = 6;

// Stand des Kippschalters
int switchState = 0;

// vorheriger Stand des Kippschalters
int prevSwitchState = 0;

// Varible zur Auswahl der Antwort
int reply;
                                      
void setup()                                    
{
  // Groesse des Displays initialisieren
  lcd.begin(16, 2);

  // Pin 6 wird auf INPUT gesetzt
  pinMode(switchPin, INPUT);

  // Textausgabe, Cursor steuern (1. Stelle, 2. Zeile), erneute Textausgabe
  lcd.print("Frage die");                       
  lcd.setCursor(0, 1);
  lcd.print("Kristallkugel!");
}

void loop()                                     
{
  // Zustand des Kippschalters abrufen
  switchState = digitalRead(switchPin);

  // Bedingung für Auswahl der Antwort - Kippschalter muss sich aendern
  if(switchState != prevSwitchState)            
  {
    if(switchState == LOW)
    {
      // Auswahlvariable wird zufällig gewählt (0 - 7)
      reply = random(8);                        
      
      // Zeichen von Display werden entfernt 
      // Cursor steuern (1. Stelle, 1.Zeile)
      // Textausgabe
      // Cursor steuern (1. Stelle, 2. Zeile)
      lcd.clear();                              
      lcd.setCursor(0, 0);
      lcd.print("Die Kugel sagt: ");
      lcd.setCursor(0, 1);
      
      // Auswahl der Antwort mit Auswahlvariable, anschließend Ausgabe der Antwort
      switch(reply)                             
      {
        case 0:
        lcd.print("Ja");
        break;
        case 1:
        lcd.print("Ganz bestimmt");
        break;
        case 2:
        lcd.print("Gewiss");
        break;
        case 3:
        lcd.print("Gute Aussichten");
        break;
        case 4:
        lcd.print("Nicht sicher");
        break;
        case 5:
        lcd.print("Frag nochmal");
        break;
        case 6:
        lcd.print("Zweifelhaft");
        break;
        case 7:
        lcd.print("Nein");
        break;
      }
    }
  }

  // vorheriger Zustand des Kippschalters wird vergessen
  // Kippschalter muss sich für eine erneute Antwort wieder aendern
  prevSwitchState = switchState;                
}
